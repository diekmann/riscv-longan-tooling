// Get everything the rust runtime needs ready. Except for memory management.
use core::panic::PanicInfo;

#[naked]
#[inline(always)]
fn hang() -> !{
    loop{
        unsafe {
            // seems to stall qemu for ever.
            asm!("wfi"); // Wait For Interrupt.
        }
    }
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    //TODO blink?

    hang();
}

#[no_mangle]
pub fn abort() -> ! {
    //TODO blink?
    hang();
}

