#![no_std]  // No implicit linking to the std lib.
#![no_main] // There is no runtime, we define our own entry point
#![feature(naked_functions)] // requires nightly
#![feature(asm)]
#![feature(core_intrinsics)] // volatile memory access

mod machine_setup;
mod rust_setup;


use core::mem::size_of;



#[no_mangle] // out boot code literally calls my_main.
#[inline(never)] // called from _start, where no stack is ready. This is the first function with a real prologue and stack setup.
pub fn my_main(){

    // TODO the following is missing, looks like this is why my code does not get out of a cold reset.
    // r0::zero_bss(&mut _sbss, &mut _ebss);
    // r0::init_data(&mut _sdata, &mut _edata, &_sidata);

    init_ports();
    blink_led();

    poweroff();

}
 
fn poweroff() {
    // https://groups.google.com/a/groups.riscv.org/forum/#!topic/sw-dev/IET9LBFJohU
    // https://github.com/qemu/qemu/blob/afd760539308a5524accf964107cdb1d54a059e3/hw/riscv/virt.c#L59
    // https://github.com/michaeljclark/riscv-probe/blob/master/libfemto/drivers/sifive_test.c
    let t: *mut u32 = 0x100000 as *mut u32;
    unsafe {
        //TODO likely not for longan board.
        core::intrinsics::volatile_store(t, 0x5555);
    }
}




const rcu_apb2en: u32 = (0x4002_1000 + 0x18);

const gpioa_ctl0: u32 = (0x4001_0800 + 0x0);
const gpioa_data: u32 = (0x4001_0800 + 0xc);
const gpioa_bop:  u32 = (0x4001_0800 + 0x10); // GPIO port bit operation register
const gpioa_bc:   u32 = (0x4001_0800 + 0x14); // GPIO bit clear register

fn init_ports() {
    unsafe {
        // Enable clock to Port A and Port C 
        let x = core::ptr::read_volatile(rcu_apb2en as *mut u32);
        core::ptr::write_volatile(rcu_apb2en as *mut u32, x | (1 << 2));

        // Enable push-pull o/p Port A, pins 1 and 2.
        let x = core::ptr::read_volatile(gpioa_ctl0 as *const u32) & !(0xf << 4);
        core::ptr::write_volatile(gpioa_ctl0 as *mut u32, x | (0x3 << 4));

        // This seems not to be necessary.
        //GPIO_BC(digitalPinToPort(pinNumber)) = digitalPinToBitMask(pinNumber);
        core::ptr::write_volatile(gpioa_bc as *mut u32, 1 << 1);
    }
}

// don't compile with optimization enabled!
fn delay(mut n: u32) {
    while n != 0 {
        n -= 1;
    }
}

// Blink Green LED (PA1).
fn blink_led() {
    //let mut bits:u32 = !(1 << 1);
    loop {
        //unsafe {
        //    // LED on when PA1 bit is 0
        //    core::ptr::write_volatile(gpioa_data as *mut u32, bits);
        //}
        unsafe {
            // HIGH
            core::ptr::write_volatile(gpioa_bop as *mut u32, 1 << 1);
        }
        delay(0x4ffff);
        //bits = !bits;

        unsafe {
            // LOW
            core::ptr::write_volatile(gpioa_bc as *mut u32, 1 << 1);
        }
        delay(0x4ffff);
    }
}
