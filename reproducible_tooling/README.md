Getting a dfu-util we can build from sources:

```
git clone git://git.code.sf.net/p/dfu-util/dfu-util
cd dfu-util
git checkout v0.9
# from https://github.com/riscv-mcu/gd32-dfu-utils
wget https://github.com/riscv-mcu/gd32-dfu-utils/commit/c2c8215061b08146e6a4c8c22bc57fae95c656df.patch
git apply c2c8215061b08146e6a4c8c22bc57fae95c656df.patch
sudo apt install autoconf build-essential libusb-1.0-0-dev
./autogen.sh
./configure
make
```

Insatlling udev rules.
From https://raw.githubusercontent.com/platformio/platformio/develop/scripts/99-platformio-udev.rules
```
sudo cp 99-platformio-udev.rules /etc/udev/rules.d/99-platformio-udev.rules
```
Reboot or reinitialized udev.
